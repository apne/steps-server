$(document).ready(function() {
    var origin = getOrigin();
    var table = $('table').DataTable({
        ajax: {
            url: origin + '/steps',
            dataSrc: ''
        },
        columns: [
            {
                data: 'user'
            },
            {
                data: 'date',
                render: function(data, type, row) {
                    if (type === "display" || type === "filter")
                        return moment(data).format('DD/MM/YYYY, HH:mm:ss');
                    else
                        return data;
                }
            },
            {
                data: 'steps'
            }
        ]
    });
    var date = new Date();
    $('div .right-justified')[0].innerText = 'Latest update: ' + moment(date.getTime()).format('DD/MM/YYYY, HH:mm:ss');


    setInterval(function() {
        table.ajax.reload(null, false);
        var date = new Date();
        $('div .right-justified')[0].innerText = 'Latest update ' + moment(date.getTime()).format('DD/MM/YYYY, HH:mm:ss');
    }, 30000);

    function getOrigin() {
        var origin = location.pathname.split('/');
        return location.protocol + '//' + location.host + '/' + origin[1];
    }

});
