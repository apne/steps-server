package msa.ait.gr.daos;

import msa.ait.gr.entities.Steps;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface StepsDAO extends CrudRepository<Steps, Long> {

    @Query("SELECT s FROM Steps s WHERE s.user = :user")
    Steps findStepsByUser(@Param("user") String user);

}
