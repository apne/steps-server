package msa.ait.gr.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by apne on 16-Jul-17.
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    String index() {
        return "../index";
    }
}
