package msa.ait.gr.controllers;

import msa.ait.gr.daos.StepsDAO;
import msa.ait.gr.entities.Steps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StepsController {
    @Autowired
    private StepsDAO stepsDAO;

    @PostMapping(path="/steps", consumes = "application/json")
    public ResponseEntity<Long> storeSteps(@RequestBody Steps steps) {
        Steps storedSteps = stepsDAO.findStepsByUser(steps.getUser());
        if (storedSteps == null)
            stepsDAO.save(steps);
        else {
            storedSteps.setDate(steps.getDate());
            storedSteps.setSteps(steps.getSteps());
            stepsDAO.save(storedSteps);
        }
        return new ResponseEntity<>(steps.getId(), HttpStatus.OK);
    }

    @GetMapping(path="/steps")
    public ResponseEntity<List<Steps>> listAllSteps() {
        List<Steps> stepsList = new ArrayList<>();
        for (Steps steps : stepsDAO.findAll())
            stepsList.add(steps);
        return new ResponseEntity<>(stepsList, HttpStatus.OK);
    }

    @GetMapping(path="/steps/{user}")
    public ResponseEntity<Steps> getSteps(@PathVariable("user") String user) {
        Steps steps = stepsDAO.findStepsByUser(user);
        return new ResponseEntity<>(steps, HttpStatus.OK);
    }
}
